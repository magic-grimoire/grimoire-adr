# 4. Gitlab

Date: 2020-11-29

## Status

Accepted

## Context

Code versioning and continuous integration have becomed standard.
As a side project, I don't need a complete tool and I want it to be free.

## Decision

Gitlab is a free platform for 
- hosting and versioning code with Git
- continuous integration
- static project website hosting

## Consequences

Lack of visibility in search engines.

