# 1. Use architecture decision records

Date: 2020-11-29

## Status

Accepted

## Context

As a side project, I want to keep track of the decisions I made.
I also want future maintainers to understand my choices.

As a professional developer, I also want to try new tools and find if they could fit with my work.

## Decision

ADR (see https://adr.github.io/) give a tooling and and a template to document architecture decisions.

## Consequences
 
As a side project, it will consume some time that I could use to actually write code.
But it will also forces me to think twice about important decisions. 
