# 5. Hexagonal architecture

Date: 2020-11-29

## Status

Accepted

## Context

I want to build multiple apps from a common code : 
- a search engine
- a desktop collection application
- a mobile application
- ...

## Decision

The Magic Grimoire will be separated in multiple repositories: 
- grimoire-adr
- grimoire-core
- grimoire-search
- grimoire-desktop
- grimoire-mobile
- ...

The Grimoire Core repository will contain the business code, written in hexagonal architecture.
It is written in pure kotlin and expose interfaces to stay abstracted from infrastructure and implementation details.

## Consequences

Creating many apps with the same business code should be easier, but the Core development may be harder.

