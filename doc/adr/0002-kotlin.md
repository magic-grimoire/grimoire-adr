# 2. Kotlin

Date: 2020-11-29

## Status

Accepted

## Context

As a side project, I want to have fun writing this application.
I also don't have much time, so the language should be productive.

## Decision

Kotlin is a refreshing language when coming from Java.
It's also very productive due to its clean syntax and the Java ecosystem.

## Consequences

New contributors would have to know of discover the language.
I will have to follow the language evolution.