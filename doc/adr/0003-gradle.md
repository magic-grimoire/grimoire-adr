# 3. Gradle

Date: 2020-11-29

## Status

Accepted

## Context

A package manager has become a standard. 
As we use Kotlin, we have the choice between the two main package managers for JVM : Maven and Gradle.
As a side project, I also want to experiment different tools.

## Decision

Gradle is both a tool I don't already use at work, and has a Kotlin DSL.

## Consequences

Learn Gradle and it's Kotlin DSL syntax.
